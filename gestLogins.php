<?php
//if (DEV_LVL>2) echo __FILE__.'<br>';
$gestLib->loadLib('gestLogins',__FILE__,'0.0.4b-dev',"gestionnaire de connexion");
$gestLib->libs['gestLogins']->git='https://git.framasoft.org/legraLibs/gestLogins';
//	$gestLib->libs['gestLogins']->setErr(LEGRALERR::DEBUG);

//ini_set('display_errors',1);ini_set('error_reporting',E_ALL);
/*!******************************************************************
fichier: login.php
auteur : Pascal TOLEDO
date :15 octobre 2012
source: http://www.legral.fr/lib/legral/php/gestLogins/
depend de:
	* _session
description:
	* gestion des utilisateurs avec leur mot de passe
documentation:
*******************************************************************/
class GestLogin{
    private $login=NULL;
    private $pwd=NULL; // public for test only
	// domaine de connection: 0:connection au domaine interdite;1:autorisé;  (2:effective) -> ???
    //public $attrs;
	public $domaines;

	function __construct($login,$pwd,$prefixCookie=''){
		$this->login=$login;
        $this->pwd=$pwd;
  		$this->domaines=array();
		//$this->domaines['admin']=0;
		//$this->attrs=new gestAttributs();
		$this->domaines['admin']=0;
	}

    // - Hydratation - //
    function getLogin(){
        return $this->login;
    }
    function setLogin($val){
        $this->login=$val;
    }

    function getPwd(){
        return $this->pwd;
    }
    function setPwd($val){
        $this->Pwd=$val;
    }

    function verifyPwd($pwd){return($pwd===$this->pwd)?1:0;}

    function setDomaine($domaine,$value=1){
        $this->domaines[$domaine]=(int)$value;
        gestLib_inspect('$this->domaines['.$domaine.']',$this->domaines[$domaine]);
    }


	// - l'user est il autorise dans ce domaine? - //
	function isDomaine($domaine){
		if($this->domaines['admin']===1)return 1;
        if (isset($this->domaines[$domaine]) AND $this->domaines[$domaine]===1  ){
            return 1;
        }
        return 0;
		}

	// - afficher, sous forme de liste, tous les domaines de l'user - //
	function showDomaines(){
		$out='<ul>';
		foreach($this->domaines as $domaineKey=>$domaineValue)
			{$out.= "<li>$domaineKey : $domaineValue</li>";}
		$out.='</ul>';
		return $out;
	}
	// - afficher, sous forme de liste, tous les domaines de l'user - //
/*	function showAttributs(){
		$out='<ul>';
		foreach($this->attrs->get() as $nom =>$val)
			{$out.= "<li>$nom : $val</li>";}
		$out.='</ul>';
		return "\n$out\n";
	}
 */
}//class gestLogin

//
// - gestion des utilisateurs - //
//
class GestLogins{

    private $users=array();

    // - au: ActualUser- //
    public $au='';	// login de l'user actuellement connecte (laisser '' pour la comparaison avec NULL)
    private $prefix='';
    private $isStayConnected=0;// rester connecte (sauve dans cookie)
    private $etat=0; // etat de la connexion: 1:connecté 0:non connecté -1:mauvais login/pwd

    function __construct($prefixe){
	    $this->prefixe=$prefixe;
        //if(!empty($_SESSION[$this->prefix.'login']))
        $this->reconnect();
	}

	function addUser($login,$pwd){
		if(isset($this->users[$login])){return -1;}
		else{$this->users[$login]=new GestLogin($login,$pwd);}
	}


    // - Connexion / Deconnexion -  //
    function whoIsConnected(){return $this->au;}


	// - renvoie 1 si un user (ou l'AU) est connecte - //
	function isConnect($userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(empty($userId))return 0;	// $u est empty bizar!
		return ($this->au===$userId)?1:0;
	}

    
    // - gestion de la connexion/deco - //

	// - reconnecter un user: recup la session pour une connexion auto - //
	function reconnect(){
        $this->setIsStayConnected(isset($_COOKIE[$this->prefixe.'isStayConnected'])?(int)$_COOKIE[$this->prefixe.'isStayConnected']:0);
		if(isset($_SESSION[$this->prefixe.'login'])){
            $this->au=$_SESSION[$this->prefixe.'login'];
			return 1;
        }
        elseif(isset($_COOKIE[$this->prefixe.'login'])){
            return $this->connect($_COOKIE[$this->prefixe.'login'],isset($_COOKIE[$this->prefixe.'pwd'])?$_COOKIE[$this->prefixe.'pwd']:NULL);
        }
		return 0;
    }

    // - connecter un user avec son login pwd - /
    // @return 1 si connecte sinon 0
    function connect($login,$pwd){
        $this->etat=0;
        if (isset($_POST['stayConnect'])){
            $this->setIsStayConnected(1);
        }
        elseif (isset($_COOKIE[$this->prefixe.'isStayConnected']) AND $_COOKIE[$this->prefixe.'isStayConnected'] === 1){
            $this->setIsStayConnected(1);
        }


            $this->setIsStayConnected(isset($_COOKIE[$this->prefixe.'isStayConnected'])?(int)$_COOKIE[$this->prefixe.'isStayConnected']:0);

		if((isset($this->users[$login]))and($this->users[$login]->verifyPwd($pwd))){
            gestOut_info(__FILE__.':'.__FUNCTION__.':'.__LINE__.': connecté');
            $this->etat=1;
            $_SESSION[$this->prefixe.'login']=$login;

            // l'utilisateur veut-il rester connecte?
            if ($this->isStayConnected === 1){
                //gestOut_info(__FILE__.':'.__FUNCTION__.':'.__LINE__.': demande de sauvegarde du login/pwd');
                $cookieDuree=time()+31104000;
                $isOk=setcookie($this->prefixe.'login',$login,$cookieDuree);
                //gestLib_inspect(__FILE__.':'.__FUNCTION__.':'.__LINE__.':$cookie login:',$isOk);
                setcookie($this->prefixe.'pwd',$pwd,$cookieDuree);
                setcookie($this->prefixe.'isStayConnected',1,$cookieDuree);
            }
            else{//non->suppression des traces precedentes de l'utilisateur
                gestOut_info(__FILE__.':'.__FUNCTION__.':'.__LINE__.': suppression des anciennes traces de sauvegarde de login/pwd');
                setcookie($this->prefixe.'login',NULL,-1);
                setcookie($this->prefixe.'pwd',NULL,-1);
            }

            $this->au=$login;return 1;
        }
        else{// mauvais login/pwd
            $this->etat=-1;
            return -1;
        }
		return 0;
	}


    function deconnect(){
        if($this->au!==''){
            // - Suppression des traces de l'Actual User - //
            $_SESSION[$this->prefixe.'login']='';$this->au='';return 1;
            gestLib_inspect(__FILE__.':'.__FUNCTION__.':'.__LINE__.':$_COOKIE',$_COOKIE);

            if ($this->isStayConnected === 0){
            gestLib_inspect(__FILE__.':'.__FUNCTION__.':'.__LINE__.':$this->isStayConnected',$this->isStayConnected);
                setcookie($this->prefixe.'login',NULL,-1);
                setcookie($this->prefixe.'pwd',NULL,-1);
            }
        }
		return 0;
    }

    
    // - Hydratation - //

    function getEtat(){
        return $this->etat;
    }

    function getIsStayConnected(){
        return $this->isStayConnected;
    }

    function setIsStayConnected($stay=1){
        $this->isStayConnected=(int)$stay;
        $_COOKIE[$this->prefixe.'isStayConnected']=(int)$stay;
    }

    function setPrefixeCookie($val=''){
        $this->prefixeCookie=$val;
    }

    function getAu(){
        return $this->au;
    }


    // - meta Function - //
    // -- renvoie le pwd de l'AU -- //
    function getPwd($userId=NULL){
        if($userId===NULL)$userId=$this->au;
        if(isset($this->users[$userId])){
            return $this->users[$userId]->getPwd();
        }
        else{
            return NULL;    // attention a la validation!
        }
    }
	// - gestion des domaines - //
	
    function isDomaine($domaine,$userId=NULL){
        if($userId===NULL)$userId=$this->au;
        if(isset($this->users[$userId])){
            return($this->users[$userId]->isDomaine($domaine));
        }
        // $userId inconnu ou non connecte
        return 0;
    }

    function setDomaine($domaine,$val=1,$userId=NULL){
        if($userId===NULL)$userId=$this->au;
        //gestLib_inspect(__LINE__.':$this->au(user exist)',$this->au);
        //gestLib_inspect(__LINE__.':$this->users['.$userId.'])',$this->users[$userId]);
        if(isset($this->users[$userId])){
            gestLib_inspect(__LINE__.':',$this->au);
            return($this->users[$userId]->setDomaine($domaine,$val));
        }
    }
	
    // - renvoie le tableau de domaine d'un ou de l'user - //
    // @return NULL si user inexistant sinon renvoie les domaines de l'user
    function getDomaines ($userId=NULL){
		if($userId===NULL)$userId=$this->au;
        if(isset($this->users[$userId]))return $this->users[$userId]->domaines;
        return NULL;
	}

    function showDomaines ($userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId]))return $this->users[$userId]->showDomaines();
	}

	// - function pour l'ActualUser(AU) - //
	function AUsetDomaine($domaine,$value=0){if(isset($this->users[$this->au]))$this->users[$this->au]->setDomaine($domaine,$value);}



	// - gestion des domaines des attributs - //
/*	function setAttr($userId,$key,$val,$force=1){
		if(isset($this->users[$userId])){$this->users[$userId]->attrs->set($key,$val,$force);}
	}
	function _unsetAttr($userId,$key){
		if(isset($this->users[$userId])){$this->users[$userId]->attrs->_unset($key);}
	}

	function getAttr($userId,$key=NULL){
		if(isset($this->users[$userId])){return $this->users[$userId]->attrs->get($key);}
	}

	function showAttributs ($userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId]))return $this->users[$userId]->showAttributs();
	}
 */




	function tableau(){
		$out ='<table class=""><caption>gestLogins</caption>';
		$out.='<thead><tr><th>login</th><th>pwd</th><th>domaines</th><th>attributs</th></tr></thead>';
		foreach($this->users as $key => $user){
			$out.='<tr>';
			$out.='<td>'.$user->login.'</td>';
			$out.='<td>'.$user->pwd.'</td>';

			$out.='<td>';
			foreach($user->domaines as $nom =>$val){
				$out.="$nom : $val<br>";
			}	
			$out.='</td>';
			$out.='<td>';
			//foreach($user->attrs->get() as $nom =>$val){
			//	$out.="$nom : $val<br>";
			//}	
			$out.='</td>';

			$out.="</tr>\n";
			};

		$out.='</table>';
		return $out;	
	}	
} // class gestLogins


////////////////////////////////////////////////////
// exemple d'utilisation
//
// creation d'une instance
//$legralUsers=new gestLogins();
//
// - ajout d'un utilisateur - //
//$legralUsers->addUser('pascal','mot_de_passe');
//
// - autoriser un domaine a un user - //
//$legralUsers->setDomaine('pascal','site');

// - interdire un domaine a un user - //
//$legralUsers->setDomaine('pascal','site',0);


// - test de l'autorisation d'un domaine pour un user - //
// if($legralUsers->isDomaine('pascal','site')){echo "l'utilisateur pascal est autoriser sur le domaine 'site' ".'<br>';}else{echo "l'utilisateur pascal N'est PAS autoriser sur le domaine 'site' ".'<br>';}

// - connecter un user avec son login/pwd - //
//if($legralUsers->connect('pascal','test0') === 1){}

// - voir qui est connecter - //
//echo 'Qui est connecter:'.$legralUsers->whoIsConnected().'<br>';

$gestLib->setEtat('gestLogins',LEGRAL_LIBETAT::LOADED);
$gestLib->end('gestLogins');
